### Demo Project:
Deploy MongoDB and Mongo Express into local K8s cluster
### Technologies used:
Kubernetes, Docker, MongoDB, Mongo Express
### Project Description:
 - Setup local K8s cluster with Minikube
 - Deploy MongoDB and MongoExpress with configuration and credentials extracted into ConfigMap and Secret
  - kubectl apply -f secret.yaml
  - kubectl apply -f configmap.yaml
 Generate username and password for Secret file as below
 echo -n 'name' | base64
 echo -n 'pass' | base64
 Assign external service a public IP address - minikube service service_name

----------------
minikube addons enable ingress
### Demo Project:
Deploy Mosquitto message broker with ConfigMap and Secret Volume Types
### Technologies used:
Kubernetes, Docker, Mosquitto
### Project Description:
 - Define configuration and passwords for Mosquitto message broker with ConfigMap and Secret Volume types
   - Mount volumes in the pod in the deployment configuration file
        volumes:
            - name: mosquitto-conf
              configMap:
                name: mosquitto-config-file
            - name: mosquitto-secret
              secret:
                secretName: mosquitto-secret-file   
   - Mount the defined volumes in the pod in the container in the deployment configuration file
        volumeMounts:
            - name: mosquitto-conf
              mountPath: /mosquitto/config
            - name: mosquitto-secret
              mountPath: /mosquitto/secret  
              readOnly: true

 ---------------

### Demo Project:
Install a stateful service (MongoDB) on Kubernetes using Helm
### Technologies used:
K8s, Helm, MongoDB, Mongo Express, Linode LKE, Linux
### Project Description:
 - Create a managed K8s cluster with Linode Kubernetes Engine
 - Deploy replicated MongoDB service in LKE cluster using a Helm chart
   - Install helm bitnami helm chart
   - Set architecture to replicaset
   - Set replicacount and auth rootpassword
   - helm install mongodb --values mongodbTest.yaml bitnami/mongodb
 - Configure data persistence for MongoDB with Linode’s cloud storage
   - Set persistance storage class to "linode-block-storage"
 - Deploy UI client Mongo Express for MongoDB
   - Configure environment variables in Mongo Express deployment
       env:
          - name: ME_CONFIG_MONGODB_ADMINUSERNAME
            value: root
          - name: ME_CONFIG_MONGODB_SERVER
            value: mongodb-0.mongodb-headless
          - name: ME_CONFIG_MONGODB_ADMINPASSWORD
            valueFrom: 
              secretKeyRef:
                name: mongodb
                key: mongodb-root-password ---> value can be found in secret volumes - part of helm charts for mongodb
 - Deploy and configure nginx ingress to access the UI application from browser
   - helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   - helm install my-nginx-ingress ingress-nginx/ingress-nginx
   - set nodebalancer ip address as the host in ingress config file.
    - host: 172-105-1-21.ip.linodeusercontent.com


 ---------------

### Demo Project:
 Deploy our web application in K8s cluster from private Docker registry
### Technologies used:
Kubernetes, Helm, AWS ECR, Docker
### Project Description:
 - Create Secret for credentials for the private Docker registry
  - Login in to aws ecr repo.
    - aws ecr get-login
    - docker login -u username -p password 
    - ssh into minikube and login to aws ecr using the above credentials - since minikube can't access your credsStore
    - A file containing the credentials is created at __.docker/config.json__ use it to create secret
    - scp -i $(minikube ssh-key) docker@$(minikube ip):.docker/config.json .docker/config.json - copy file to your host
    - cat .docker/config.json | base64
 - Configure the Docker registry secret in application Deployment component
 - Deploy web application image from our private Docker registry in K8s cluster

----------------

### Demo Project:
Setup Prometheus Monitoring in Kubernetes cluster
### Technologies used:
Kubernetes, Helm, Prometheus
### Project Description:
 - Deploy Prometheus in local Kubernetes cluster using a Helm chart
    __add repos__
      helm repo add stable https://kubernetes-charts.storage.googleapis.com/
    __install chart__
      helm install prometheus prometheus-community/kube-prometheus-stack


----------------
### Demo Project:
Deploy Microservices application in Kubernetes with
Production & Security Best Practices
### Technologies used:
Kubernetes, Redis, Linux, Linode LKE
### Project Description:
 - Create K8s manifests for Deployments and Services for all microservices of an online shop application
 - Deploy microservices to Linode’s managed Kubernetes cluster

----------------

### Demo Project:
Create Helm Chart for Microservices
### Technologies used:
Kubernetes, Helm
### Project Description:
 - Create 1 shared Helm Chart for all microservices, to reuse common Deployment and Service configurations for the services

----------------

### Demo Project:
Deploy Microservices with Helmfile
### Technologies used:
Kubernetes, Helm, Helmfile
### Project Description:
 - Deploy Microservices with Helm
 - Deploy Microservices with Helmfile

----------------

Module 10: Container Orchestration with Kubernetes



