### Demo Project:
Install a stateful service (MongoDB) on Kubernetes using Helm
### Technologies used:
K8s, Helm, MongoDB, Mongo Express, Linode LKE, Linux
### Project Description:
 - Create a managed K8s cluster with Linode Kubernetes Engine
 - Deploy replicated MongoDB service in LKE cluster using a Helm chart
   - Install helm bitnami helm chart
   - Set architecture to replicaset
   - Set replicacount and auth rootpassword
   - helm install mongodb --values mongodbTest.yaml bitnami/mongodb
 - Configure data persistence for MongoDB with Linode’s cloud storage
   - Set persistance storage class to "linode-block-storage"
 - Deploy UI client Mongo Express for MongoDB
   - Configure environment variables in Mongo Express deployment
       env:
          - name: ME_CONFIG_MONGODB_ADMINUSERNAME
            value: root
          - name: ME_CONFIG_MONGODB_SERVER
            value: mongodb-0.mongodb-headless
          - name: ME_CONFIG_MONGODB_ADMINPASSWORD
            valueFrom: 
              secretKeyRef:
                name: mongodb
                key: mongodb-root-password ---> value can be found in secret volumes - part of helm charts for mongodb
 - Deploy and configure nginx ingress to access the UI application from browser
   - helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   - helm install my-nginx-ingress ingress-nginx/ingress-nginx
   - set nodebalancer ip address as the host in ingress config file.
    - host: 172-105-1-21.ip.linodeusercontent.com